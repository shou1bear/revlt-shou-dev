# Gathered data for game development
---

## Historical Data
---

### Start

 Prologue - Arrest of Jose Rizal
			Founding of KKK
---

### End

 Epilogue - Dissolution of the Katipunan

---

### Chapters

 1. Prologue
 2. Chapter 1: Preparation + Tutorial
 3. Chapter 2: Cry of Pugad Lawin
 4. Chapter 3: Battle of San Juan del Monte
 5. Chapter 4: Cry of Nueva Ecija
 6. Chapter 5: Battle of Talisay
 7. Chapter 6: 
 10. Final Chapter: Battle of Aliaga
---




## Philippine Revolution Battles (By Date)
---

### 1896

1. Cry of Pugad Lawin - August							Chapter 1 jpg resrcs https://rizalmemories.wordpress.com/2016/10/20/rizal-the-day-of-execution/
2. Battle of Pasong Tamo - August 28-29
3. Battle of Manila - August 29
4. Battle of San Juan del Monte - August 30				Chapter 2 jpg resrcs https://philippinestravelsite.com/august-festivals-philippines/
5. Battle of Noveleta - August 30
6. Battle of San Francisco de Malabon - August 31
7. Kawit Revolt - August 31
8. Cry of Nueva Ecija - September 2-3					Chapter 3  jpg res. https://wikivisually.com/wiki/Cry_of_Nueva_Ecija
9. Battle of Imus - September 1-3
10. Battle of Talisay - October 12
11. Battles of Batangas - October 23
12. Battle of Binakayan-Dalahican - November 9–11		Chapter 4  jpg res https://explora.ph/attraction/82/battle-of-binakayan-monument#&gid=1&pid=5
13. Battle of Sambat - November 15–16
14. Battle of San Mateo and Montalban - November 7
15. Battle of San Rafael - November 30
16. 1896 Manila mutiny - December 5						Chapter 5  jpg res https://www.britannica.com/event/Battle-of-Manila-1899
17. Battle of Pateros - December 31-January 3
---

### 1897

1. Cry of Tarlac - January 24							Chapter 6    https://wikivisually.com/wiki/Cry_of_Tarlac
2. Battle of Kakarong de Sili - January 1
3. Battle of Zapote Bridge - February 17
4. Battle of Perez Dasmariñas - February 15-March 24	Chapter 7   jpog res https://wikivisually.com/wiki/Retreat_to_Montalban
5. Retreat to Montalban - May 3
6. Retreat to Biak-na-Bato - August 	
7. Battle of Aliaga - September 5–6						Chapter 8   jpg res https://www.bayaniart.com/heneral-gregorio-del-pilar/ bayani-art-base-general-gregorio-del-pilar-battle-of-tirad-pass/
---

### Extras

1. Battle of Calamba - May 1898
2. Battle of Alapan - May 28, 1898
3. Battle of Tayabas - May 28 – June 15, 1898
4. Siege of Baler - 1 July 1898 – 2 June 1899
5. Battle of Manila (1898) - 13 August 1898
6. Siege of Masbate - August 19, 1898
7. Revolt of Ambos Camarines - September 17, 1898
---

### Visayas

1. Battle of Tres de Abril - 3–8 April 1898
2. Negros Revolution - November 3, 1898 - November 6, 1898
3. Battle of Antique - September 21, 1898 - November 23, 1898
4. Battle of Barrio Yoting - December 3, 1898
5. Battle of Sapong Hills - December, 1898
---

### Mindanao

1. Siege of Zamboanga - April 27, 1899 – May 18, 1899
---

## Summaries

Prologue:

Late 19th Century

The Age of Revolutions have come and gone, mainland Europe have experienced changes that has led to drastic changes to the lives of European citizens.

These changes also affected Spain. During the 19th century, Spain was invaded by Napoleon Bonaparte of France and placed his brother on the Spanish throne. This sparked a crisis of legitimacy of crown rule from continental Europe. This led to Spain losing most of their colonies in the Americas and separatist movements in Spain.

Meanwhile in the Philippines, winds of liberalism arrived with the appointment of Governor-General Carlos María de la Torre. During his reign, he enforced liberal laws that made him the most beloved Governor-General of the Philippines. He was later replaced by Rafael Izquierdo y Gutiérrez, who was described as the opposite of the liberal-mindedness of the previous Governor-General.

It was during his rule that the unsuccessful 1872 Cavite Munity happened. The Spanish authority cracked down on the sprouting Philippine nationalism which led to the execution of the GOMBURZA. 

Our national hero, Jose Rizal, was a member of the Propaganda Movement in Spain that strives to condemn Spanish abuses and seeks for reforms in the colonial government. He later founded La Liga Filipina for the purpose of seeking these reforms. 

Days after the establishment of the organization, Rizal was deported to Dapitan. Andres Bonifacio, a radical member of La Liga Filipina, then established a secret organization that has the goal of independence from Spanish rule in the Philippines, that was the Katipunan.



Chapter 1: The Cry of Pugad Lawin

-



Chapter 2: 

-


Chapter 3:

-


Chapter 4:

-


Chapter 5:

-


Chapter 6:

-


Chapter 7:

-


Chapter 8:

-


Epilogue:

-
---